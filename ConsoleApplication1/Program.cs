﻿using Impi.Assembly.Content.Sort;
using Impi.Assembly.Core.Ioc;
using System;

namespace ConsoleApplication1
{
    class Program
    {
        private static SortServiceImpl HomeVoucherSortService { get; set; }

        private class HomeVoucherSortConfig : SortConfigImpl
        {
            public string DbName { get { return "content"; } }

            public string TableName { get { return "content_homevouchersort"; } }
        }

        static Program()
        {
            HomeVoucherSortService = new IocContainer().RegisterType<HomeVoucherSortConfig, SortConfigImpl>().Register(SortRegister.Register).Resolve<SortServiceImpl>();
        }

        static void Main(string[] args)
        {


            Console.Read();
        }
    }
}