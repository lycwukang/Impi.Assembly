﻿namespace Impi.Assembly.Content.Image.Carousel
{
    /// <summary>
    /// 轮播图
    /// </summary>
    public class CarouselImageModel : ImageModel
    {
        /// <summary>
        /// 图片链接
        /// </summary>
        public string Uri { get; set; }
    }
}