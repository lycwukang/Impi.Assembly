﻿using Impi.Assembly.Core.Ioc;

namespace Impi.Assembly.Content.Image.Carousel
{
    /// <summary>
    /// 模块注册
    /// </summary>
    public class CarouselImageRegister
    {
        /// <summary>
        /// 创建模块
        /// </summary>
        /// <param name="iocContainer"></param>
        public static void Register(IocContainer iocContainer)
        {
            if (!iocContainer.IsRegister<CarouselImageServiceImpl>()) iocContainer.RegisterType<CarouselImageService, CarouselImageServiceImpl>();
        }
    }
}