﻿using System;

namespace Impi.Assembly.Content.Image.Carousel
{
    public class CarouselImageExcpetion : Exception
    {
        public CarouselImageExcpetion(string message) : base(message)
        {
        }
    }
}