﻿using Impi.Assembly.Content.Sort;
using System.Collections.Generic;
using System.Linq;

namespace Impi.Assembly.Content.Image.Carousel
{
    /// <summary>
    /// 轮播图服务接口默认实现
    /// </summary>
    public class CarouselImageService : CarouselImageServiceImpl
    {
        private ImageServiceImpl _imageService = null;
        private SortServiceImpl _sortService = null;

        public CarouselImageService(ImageServiceImpl imageService, SortServiceImpl sortService)
        {
            _imageService = imageService;
            _sortService = sortService;
        }

        /// <summary>
        /// 获取轮播图
        /// </summary>
        /// <param name="contentType"></param>
        /// <param name="contentId"></param>
        /// <returns></returns>
        public List<CarouselImageModel> GetCarouselImage(int contentType, int contentId)
        {
            var images = _sortService.GetSort<int, int, CarouselImageModel>(contentType, contentId);
            return images.Select(p =>
            {
                var imageInfo = _imageService.GetImage(p.ImageId);

                return new CarouselImageModel()
                {
                    HashValue = imageInfo.HashValue,
                    Height = imageInfo.Height,
                    Host = imageInfo.Host,
                    ImageId = imageInfo.ImageId,
                    Name = imageInfo.Name,
                    Path = imageInfo.Path,
                    Subfix = imageInfo.Subfix,
                    Width = imageInfo.Width,
                    Uri = p.Uri
                };
            }).ToList();
        }

        /// <summary>
        /// 设置轮播图
        /// </summary>
        /// <param name="contentType"></param>
        /// <param name="contentId"></param>
        /// <param name="carouselImages"></param>
        public void SetCarouselImage(int contentType, int contentId, List<CarouselImageModel> carouselImages)
        {
            var images = new List<object>();
            foreach (var image in carouselImages)
            {
                images.Add(new
                {
                    ImageId = image.ImageId,
                    Uri = image.Uri
                });
            }
            _sortService.SetSort(contentType, contentId, images);
        }
    }
}