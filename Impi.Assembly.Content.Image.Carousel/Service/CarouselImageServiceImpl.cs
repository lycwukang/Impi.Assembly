﻿using System.Collections.Generic;

namespace Impi.Assembly.Content.Image.Carousel
{
    /// <summary>
    /// 轮播图服务接口
    /// </summary>
    public interface CarouselImageServiceImpl
    {
        /// <summary>
        /// 设置轮播图
        /// </summary>
        /// <param name="contentType"></param>
        /// <param name="contentId"></param>
        /// <param name="carouselImages"></param>
        void SetCarouselImage(int contentType, int contentId, List<CarouselImageModel> carouselImages);

        /// <summary>
        /// 获取轮播图
        /// </summary>
        /// <param name="contentType"></param>
        /// <param name="contentId"></param>
        /// <returns></returns>
        List<CarouselImageModel> GetCarouselImage(int contentType, int contentId);
    }
}