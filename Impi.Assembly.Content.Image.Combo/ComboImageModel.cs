﻿using System.Collections.Generic;

namespace Impi.Assembly.Content.Image.Combo
{
    /// <summary>
    /// 组合图
    /// </summary>
    public class ComboImageModel : ImageModel
    {
        /// <summary>
        /// 组合图编号
        /// </summary>
        public int ComboImageId { get; set; }

        /// <summary>
        /// 组合图名称
        /// </summary>
        public string ComboName { get; set; }

        /// <summary>
        /// 组合图触摸点
        /// </summary>
        public List<ComboImageTouchRangeModel> TouchRanges { get; set; } = new List<ComboImageTouchRangeModel>();
    }
}