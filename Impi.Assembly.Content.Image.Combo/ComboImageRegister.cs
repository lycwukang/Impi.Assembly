﻿using Impi.Assembly.Core.Ioc;

namespace Impi.Assembly.Content.Image.Combo
{
    /// <summary>
    /// 模块注册
    /// </summary>
    public static class ComboImageRegister
    {
        /// <summary>
        /// 创建模块
        /// </summary>
        /// <param name="iocContainer"></param>
        public static void Register(IocContainer iocContainer)
        {
            if (!iocContainer.IsRegister<ComboImageConfigImple>()) iocContainer.RegisterType<ComboImageConfigDefault, ComboImageConfigImple>();
            if (!iocContainer.IsRegister<ComboImageDaoImple>()) iocContainer.RegisterType<ComboImageDaoDefault, ComboImageDaoImple>();

            if (!iocContainer.IsRegister<ComboImageServiceImpl>()) iocContainer.RegisterType<ComboImageService, ComboImageServiceImpl>();
        }
    }
}