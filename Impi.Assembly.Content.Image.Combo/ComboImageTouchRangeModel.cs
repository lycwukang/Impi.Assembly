﻿namespace Impi.Assembly.Content.Image.Combo
{
    /// <summary>
    /// 组合图触摸点
    /// </summary>
    public class ComboImageTouchRangeModel
    {
        /// <summary>
        /// 开始点
        /// </summary>
        public int BeginX { get; set; }

        /// <summary>
        /// 开始点
        /// </summary>
        public int BeginY { get; set; }

        /// <summary>
        /// 结束点
        /// </summary>
        public int EndX { get; set; }

        /// <summary>
        /// 结束点
        /// </summary>
        public int EndY { get; set; }

        /// <summary>
        /// 链接
        /// </summary>
        public string Uri { get; set; }
    }
}