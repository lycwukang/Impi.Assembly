﻿namespace Impi.Assembly.Content.Image.Combo
{
    /// <summary>
    /// 组合图模块配置
    /// </summary>
    public class ComboImageConfigDefault : ComboImageConfigImple
    {
        /// <summary>
        /// 数据库配置名称
        /// </summary>
        public string DbName { get { return "content"; } }

        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get { return "content_comboimage"; } }
    }
}