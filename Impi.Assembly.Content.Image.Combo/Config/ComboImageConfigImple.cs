﻿namespace Impi.Assembly.Content.Image.Combo
{
    /// <summary>
    /// 组合图模块配置
    /// </summary>
    public interface ComboImageConfigImple
    {
        /// <summary>
        /// 数据库配置名称
        /// </summary>
        string DbName { get; }

        /// <summary>
        /// 表名
        /// </summary>
        string TableName { get; }
    }
}