﻿namespace Impi.Assembly.Content.Image.Combo
{
    /// <summary>
    /// 组合图数据存储默认实现
    /// </summary>
    public class ComboImageDaoDefault : ComboImageDaoMySql
    {
        public ComboImageDaoDefault(ComboImageConfigImple comboImageConfig) : base(comboImageConfig)
        {
        }
    }
}