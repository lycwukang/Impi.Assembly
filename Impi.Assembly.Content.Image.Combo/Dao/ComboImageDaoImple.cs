﻿namespace Impi.Assembly.Content.Image.Combo
{
    /// <summary>
    /// 组合图数据存储接口
    /// </summary>
    public interface ComboImageDaoImple
    {
        /// <summary>
        /// 添加组合图
        /// </summary>
        /// <param name="comboImage"></param>
        void AddComboImage(ComboImageDataModel comboImage);

        /// <summary>
        /// 获取组合图
        /// </summary>
        /// <param name="comboImageId"></param>
        ComboImageDataModel GetComboImage(int comboImageId);

        /// <summary>
        /// 更新组合图
        /// </summary>
        /// <param name="comboImage"></param>
        void ModifyComboImage(ComboImageDataModel comboImage);

        /// <summary>
        /// 删除组合图
        /// </summary>
        /// <param name="comboImageId"></param>
        void RemoveComboImage(int comboImageId);
    }
}