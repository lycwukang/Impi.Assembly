﻿using Impi.Assembly.Core.Data;
using System;

namespace Impi.Assembly.Content.Image.Combo
{
    /// <summary>
    /// 组合图数据存储MySql实现
    /// </summary>
    public class ComboImageDaoMySql : ComboImageDaoImple
    {
        private ComboImageConfigImple _comboImageConfig = null;

        public ComboImageDaoMySql(ComboImageConfigImple comboImageConfig)
        {
            _comboImageConfig = comboImageConfig;
        }

        /// <summary>
        /// 添加组合图
        /// </summary>
        /// <param name="comboImage"></param>
        public void AddComboImage(ComboImageDataModel comboImage)
        {
            var sql = $@"INSERT INTO `{_comboImageConfig.TableName}`(`ComboName`, `ImageId`, `TouchRanges`) VALUES(@ComboName, @ImageId, @TouchRanges);";
            var command = Commands.GetCommand(sql, _comboImageConfig.DbName);
            command.Exec(new
            {
                ComboName = comboImage.ComboName,
                ImageId = comboImage.ImageId,
                TouchRanges = comboImage.TouchRanges
            });
        }

        /// <summary>
        /// 获取组合图
        /// </summary>
        /// <param name="comboImageId"></param>
        /// <returns></returns>
        public ComboImageDataModel GetComboImage(int comboImageId)
        {
            var sql = $@"SELECT * FROM `{_comboImageConfig.TableName}` WHERE `ComboImage`=@ComboImage LIMIT 1;";
            var command = Commands.GetCommand(sql, _comboImageConfig.DbName);
            return command.Read<ComboImageDataModel>(new
            {
                ComboImage = comboImageId
            });
        }

        /// <summary>
        /// 更新组合图
        /// </summary>
        /// <param name="comboImage"></param>
        public void ModifyComboImage(ComboImageDataModel comboImage)
        {
            var sql = $@"UPDATE `{_comboImageConfig.TableName}` SET `ComboName`=@ComboName, `ImageId`=@ImageId, `TouchRanges`=@TouchRanges WHERE `ComboImageId`=@ComboImageId;";
            var command = Commands.GetCommand(sql, _comboImageConfig.DbName);
            command.Exec(new
            {
                ComboName = comboImage.ComboName,
                ComboImageId = comboImage.ComboImageId,
                ImageId = comboImage.ImageId,
                TouchRanges = comboImage.TouchRanges
            });
        }

        /// <summary>
        /// 删除组合图
        /// </summary>
        /// <param name="comboImageId"></param>
        public void RemoveComboImage(int comboImageId)
        {
            var sql = $@"DELETE FROM `content_comboimage` WHERE `ComboImageId`=@ComboImageId;";
            var command = Commands.GetCommand(sql, _comboImageConfig.DbName);
            command.Exec(new
            {
                ComboImageId = comboImageId
            });
        }
    }
}