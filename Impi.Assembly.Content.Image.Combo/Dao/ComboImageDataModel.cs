﻿namespace Impi.Assembly.Content.Image.Combo
{
    /// <summary>
    /// 组合图
    /// </summary>
    public class ComboImageDataModel
    {
        /// <summary>
        /// 组合图编号
        /// </summary>
        public int ComboImageId { get; set; }

        /// <summary>
        /// 组合图名称
        /// </summary>
        public string ComboName { get; set; }

        /// <summary>
        /// 图片编号
        /// </summary>
        public int ImageId { get; set; }

        /// <summary>
        /// 组合图触摸点
        /// </summary>
        public string TouchRanges { get; set; }
    }
}