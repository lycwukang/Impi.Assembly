﻿using System;

namespace Impi.Assembly.Content.Image.Combo
{
    public class ComboImageException : Exception
    {
        public ComboImageException(string message) : base(message)
        {
        }
    }
}