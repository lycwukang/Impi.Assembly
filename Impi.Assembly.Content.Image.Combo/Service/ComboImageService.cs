﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Impi.Assembly.Content.Image.Combo
{
    /// <summary>
    /// 组合图服务接口
    /// </summary>
    public class ComboImageService : ComboImageServiceImpl
    {
        private ComboImageDaoImple _comboImageDao = null;
        private ImageServiceImpl _imageService = null;

        public ComboImageService(ComboImageDaoImple comboImageDao, ImageServiceImpl imageService)
        {
            _comboImageDao = comboImageDao;
            _imageService = imageService;
        }

        /// <summary>
        /// 添加一张组合图
        /// </summary>
        /// <param name="comboImage"></param>
        public void AddComboImage(ComboImageModel comboImage)
        {
            _comboImageDao.AddComboImage(new ComboImageDataModel()
            {
                ComboName = comboImage.ComboName,
                ImageId = comboImage.ImageId,
                TouchRanges = JsonConvert.SerializeObject(comboImage.TouchRanges)
            });
        }

        /// <summary>
        /// 获取组合图
        /// </summary>
        /// <param name="comboImageId"></param>
        /// <returns></returns>
        public ComboImageModel GetComboImage(int comboImageId)
        {
            var model = _comboImageDao.GetComboImage(comboImageId);
            var imageInfo = _imageService.GetImage(model.ImageId);
            return new ComboImageModel()
            {
                ComboName = model.ComboName,
                ComboImageId = model.ComboImageId,
                HashValue = imageInfo.HashValue,
                Height = imageInfo.Height,
                ImageId = imageInfo.ImageId,
                Host = imageInfo.Host,
                Name = imageInfo.Name,
                Path = imageInfo.Path,
                Subfix = imageInfo.Subfix,
                Width = imageInfo.Width,
                TouchRanges = JsonConvert.DeserializeObject<List<ComboImageTouchRangeModel>>(model.TouchRanges)
            };
        }

        /// <summary>
        /// 更新组合图
        /// </summary>
        /// <param name="comboImage"></param>
        public void ModifyComboImage(ComboImageModel comboImage)
        {
            _comboImageDao.ModifyComboImage(new ComboImageDataModel()
            {
                ComboName = comboImage.ComboName,
                ComboImageId = comboImage.ComboImageId,
                ImageId = comboImage.ImageId,
                TouchRanges = JsonConvert.SerializeObject(comboImage.TouchRanges)
            });
        }

        /// <summary>
        /// 删除组合图
        /// </summary>
        /// <param name="comboImageId"></param>
        public void RemoveComboImage(int comboImageId)
        {
            _comboImageDao.RemoveComboImage(comboImageId);
        }
    }
}