﻿namespace Impi.Assembly.Content.Image.Combo
{
    /// <summary>
    /// 组合图服务接口
    /// </summary>
    public interface ComboImageServiceImpl
    {
        /// <summary>
        /// 添加一张组合图
        /// </summary>
        /// <param name="comboImage"></param>
        void AddComboImage(ComboImageModel comboImage);

        /// <summary>
        /// 获取组合图
        /// </summary>
        /// <param name="comboImageId"></param>
        /// <returns></returns>
        ComboImageModel GetComboImage(int comboImageId);

        /// <summary>
        /// 更新组合图
        /// </summary>
        /// <param name="comboImage"></param>
        void ModifyComboImage(ComboImageModel comboImage);

        /// <summary>
        /// 删除组合图
        /// </summary>
        /// <param name="comboImageId"></param>
        void RemoveComboImage(int comboImageId);
    }
}