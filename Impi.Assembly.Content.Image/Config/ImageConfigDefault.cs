﻿namespace Impi.Assembly.Content.Image
{
    /// <summary>
    /// 图片存储默认配置
    /// </summary>
    public class ImageConfigDefault : ImageConfigImpl
    {
        /// <summary>
        /// 数据库配置名称
        /// </summary>
        public string DbName { get { return "content"; } }

        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get { return "content_image"; } }

        /// <summary>
        /// AliOSS 配置
        /// </summary>
        public string AliOssAccessKeyId { get { return ""; } }

        /// <summary>
        /// AliOSS 配置
        /// </summary>
        public string AliOssAccessKeySecret { get { return ""; } }

        /// <summary>
        /// AliOSS 配置
        /// </summary>
        public string AliOssEndpoint { get { return ""; } }

        /// <summary>
        /// AliOSS 配置
        /// </summary>
        public string AliOssImageBucketName { get { return ""; } }

        /// <summary>
        /// AliOSS 配置 - 图片服务器地址
        /// </summary>
        public string AliOssImageService { get { return ""; } }
    }
}