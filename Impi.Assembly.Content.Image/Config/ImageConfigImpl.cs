﻿namespace Impi.Assembly.Content.Image
{
    /// <summary>
    /// 图片存储配置
    /// </summary>
    public interface ImageConfigImpl
    {
        /// <summary>
        /// 数据库配置名称
        /// </summary>
        string DbName { get; }

        /// <summary>
        /// 表名
        /// </summary>
        string TableName { get; }

        /// <summary>
        /// AliOSS 配置
        /// </summary>
        string AliOssAccessKeyId { get; }

        /// <summary>
        /// AliOSS 配置
        /// </summary>
        string AliOssAccessKeySecret { get; }

        /// <summary>
        /// AliOSS 配置
        /// </summary>
        string AliOssEndpoint { get; }

        /// <summary>
        /// AliOSS 配置
        /// </summary>
        string AliOssImageBucketName { get; }

        /// <summary>
        /// AliOSS 配置 - 图片服务器地址
        /// </summary>
        string AliOssImageService { get; }
    }
}