﻿namespace Impi.Assembly.Content.Image
{
    /// <summary>
    /// 图片信息存储默认实现
    /// </summary>
    public class ImageDaoDefault : ImageDaoMySql
    {
        public ImageDaoDefault(ImageConfigImpl imageConfig) : base(imageConfig)
        {
        }
    }
}