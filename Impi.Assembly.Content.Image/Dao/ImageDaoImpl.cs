﻿namespace Impi.Assembly.Content.Image
{
    /// <summary>
    /// 图片信息存储
    /// </summary>
    public interface ImageDaoImpl
    {
        /// <summary>
        /// 创建图片信息
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        int CreateImage(ImageDataModel image);

        /// <summary>
        /// 获取图片信息
        /// </summary>
        /// <param name="imageId"></param>
        /// <returns></returns>
        ImageDataModel GetImage(int imageId);
    }
}