﻿using Impi.Assembly.Core.Data;

namespace Impi.Assembly.Content.Image
{
    /// <summary>
    /// 图片信息存储MySql实现
    /// </summary>
    public class ImageDaoMySql : ImageDaoImpl
    {
        private ImageConfigImpl _imageConfig = null;

        public ImageDaoMySql(ImageConfigImpl imageConfig)
        {
            _imageConfig = imageConfig;
        }

        /// <summary>
        /// 创建图片信息
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public int CreateImage(ImageDataModel image)
        {
            var sql = $@"INSERT INTO `{_imageConfig.TableName}`(`Path`, `Name`, `Subfix`, `HashValue`, `Width`, `Height`, `CreatedAt`)
VALUES(@Path, @Name, @Subfix, @HashValue, @Width, @Height, NOW());
SELECT LAST_INSERT_ID();";
            var command = Commands.GetCommand(sql, _imageConfig.DbName);
            return command.Read<int>(new
            {
                Path = image.Path,
                Name = image.Name,
                Subfix = image.Subfix,
                HashValue = image.HashValue,
                Width = image.Width,
                Height = image.Height
            });
        }

        /// <summary>
        /// 获取图片信息
        /// </summary>
        /// <param name="imageId"></param>
        /// <returns></returns>
        public ImageDataModel GetImage(int imageId)
        {
            var sql = $@"SELECT * FROM `{_imageConfig.TableName}` WHERE `ImageId`=@ImageId LIMIT 1;";
            var command = Commands.GetCommand(sql, _imageConfig.DbName);
            return command.Read<ImageDataModel>(new
            {
                ImageId = imageId
            });
        }
    }
}