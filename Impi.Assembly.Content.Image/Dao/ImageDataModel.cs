﻿namespace Impi.Assembly.Content.Image
{
    /// <summary>
    /// 图片
    /// </summary>
    public class ImageDataModel
    {
        /// <summary>
        /// 图片编号
        /// </summary>
        public int ImageId { get; set; }

        /// <summary>
        /// 图片地址
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// 图片名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 图片后缀名
        /// </summary>
        public string Subfix { get; set; }

        /// <summary>
        /// 图片哈希值
        /// </summary>
        public string HashValue { get; set; }

        /// <summary>
        /// 图片宽度
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// 图片高度
        /// </summary>
        public int Height { get; set; }
    }
}