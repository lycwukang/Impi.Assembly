﻿using System;

namespace Impi.Assembly.Content.Image
{
    /// <summary>
    /// Impi.Assembly.Content.Image 错误
    /// </summary>
    public class ImageException : Exception
    {
        public ImageException(string message) : base(message)
        {
        }
    }
}