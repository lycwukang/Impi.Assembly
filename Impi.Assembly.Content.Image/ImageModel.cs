﻿using System;

namespace Impi.Assembly.Content.Image
{
    /// <summary>
    /// 图片信息
    /// </summary>
    public class ImageModel : ImageDataModel
    {
        /// <summary>
        /// 图片服务器
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// 图片访问路径
        /// </summary>
        public string ImageUrl
        {
            get
            {
                return Host + Path + Name + Subfix;
            }
        }

        /// <summary>
        /// 获取等比缩放的图片访问路径（短边）
        /// </summary>
        /// <param name="imageInfo"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public string GetAliOssRatioNarrowShortImageUri(int width, int height)
        {
            return $@"{ImageUrl}@{height}h_{width}w_1e";
        }

        /// <summary>
        /// 获取等比缩放的图片访问路径（长边）
        /// </summary>
        /// <param name="imageInfo"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public string GetAliOssRatioNarrowImageUri(int width, int height)
        {
            return $@"{ImageUrl}@{height}h_{width}w_0e";
        }

        /// <summary>
        /// 裁剪中间矩形区域
        /// </summary>
        /// <param name="imageInfo"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        public string GetAliOssCenterRectangleImageUri(int width)
        {
            var min = Math.Min(Width, Height);
            return $@"{ImageUrl}@{width}h_{width}w_0e_{min}x{min}-5rc";
        }

        /// <summary>
        /// 获取宽度缩放的图片访问路径
        /// </summary>
        /// <param name="imageInfo"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        public string GetAliOssWidthRatioNarrowImageUri(int width)
        {
            return $@"{ImageUrl}@{width}w_0e";
        }

        /// <summary>
        /// 获取等比缩放的圆图片访问路径
        /// </summary>
        /// <param name="imageInfo"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        public string GetAliOssRationNarrowCircularImageUri(int width)
        {
            return $@"{ImageUrl}@{width}h_{width}w_1e_{width / 2}-1ci";
        }
    }
}