﻿using Impi.Assembly.Core.Ioc;

namespace Impi.Assembly.Content.Image
{
    /// <summary>
    /// 模块创建工具
    /// </summary>
    public static class ImageRegister
    {
        /// <summary>
        /// 创建模块
        /// </summary>
        /// <param name="iocContainer"></param>
        public static void Register(IocContainer iocContainer)
        {
            if (!iocContainer.IsRegister<ImageConfigImpl>()) iocContainer.RegisterType<ImageConfigDefault, ImageConfigImpl>();
            if (!iocContainer.IsRegister<ImageDaoImpl>()) iocContainer.RegisterType<ImageDaoDefault, ImageDaoImpl>();
            if (!iocContainer.IsRegister<ImageStorageImpl>()) iocContainer.RegisterType<ImageStorageDefault, ImageStorageImpl>();

            if (!iocContainer.IsRegister<ImageServiceImpl>()) iocContainer.RegisterType<ImageService, ImageServiceImpl>();
        }
    }
}