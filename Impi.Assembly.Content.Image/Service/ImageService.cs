﻿using System;
using System.IO;
using System.Web;

namespace Impi.Assembly.Content.Image
{
    /// <summary>
    /// 图片服务实现
    /// </summary>
    public class ImageService : ImageServiceImpl
    {
        private ImageConfigImpl _imageConfig = null;
        private ImageDaoImpl _imageDao = null;
        private ImageStorageImpl _imageStorage = null;

        public ImageService(ImageConfigImpl imageConfig, ImageDaoImpl imageDao, ImageStorageImpl imageStorage)
        {
            _imageConfig = imageConfig;
            _imageDao = imageDao;
            _imageStorage = imageStorage;
        }

        /// <summary>
        /// 获取图片
        /// </summary>
        /// <param name="imageId"></param>
        /// <returns></returns>
        public ImageModel GetImage(int imageId)
        {
            var image = _imageDao.GetImage(imageId);

            return new ImageModel()
            {
                ImageId = image.ImageId,
                Width = image.Width,
                Subfix = image.Subfix,
                Path = image.Path,
                HashValue = image.HashValue,
                Height = image.Height,
                Name = image.Name,
                Host = _imageConfig.AliOssImageService
            };
        }

        /// <summary>
        /// 保存图片
        /// </summary>
        /// <param name="path"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public int SaveImage(string path, HttpContext context)
        {
            if (context.Request.Files.Count <= 0) throw new ImageException("请上传图片");

            var file = context.Request.Files[0];

            return SaveImage(path, Guid.NewGuid().ToString("n"), file);
        }

        /// <summary>
        /// 保存图片
        /// </summary>
        /// <param name="path"></param>
        /// <param name="name"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        public int SaveImage(string path, string name, HttpPostedFile file)
        {
            var subfix = file.FileName.Substring(file.FileName.IndexOf("."));

            return SaveImage(path, name, subfix, file.ContentType, file.InputStream);
        }

        /// <summary>
        /// 保存图片
        /// </summary>
        /// <param name="path"></param>
        /// <param name="name"></param>
        /// <param name="subfix"></param>
        /// <param name="contentType"></param>
        /// <param name="fileStream"></param>
        /// <returns></returns>
        public int SaveImage(string path, string name, string subfix, string contentType, Stream fileStream)
        {
            byte[] filebt = new byte[fileStream.Length];
            fileStream.Read(filebt, 0, filebt.Length);
            _imageStorage.SaveImage(path.Substring(1) + name + subfix, contentType, new MemoryStream(filebt));

            System.Drawing.Image image = System.Drawing.Image.FromStream(new MemoryStream(filebt));
            return SaveImage(path, name, subfix, null, image.Width, image.Height);
        }

        /// <summary>
        /// 保存图片
        /// </summary>
        /// <param name="path"></param>
        /// <param name="name"></param>
        /// <param name="subfix"></param>
        /// <param name="hashValue"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public int SaveImage(string path, string name, string subfix, string hashValue, int width, int height)
        {
            return _imageDao.CreateImage(new ImageDataModel()
            {
                HashValue = hashValue,
                Height = height,
                Name = name,
                Path = path,
                Subfix = subfix,
                Width = width
            });
        }
    }
}