﻿using System.IO;
using System.Web;

namespace Impi.Assembly.Content.Image
{
    /// <summary>
    /// 图片服务
    /// </summary>
    public interface ImageServiceImpl
    {
        /// <summary>
        /// 保存图片
        /// </summary>
        /// <param name="path"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        int SaveImage(string path, HttpContext context);

        /// <summary>
        /// 保存图片
        /// </summary>
        /// <param name="path"></param>
        /// <param name="name"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        int SaveImage(string path, string name, HttpPostedFile file);

        /// <summary>
        /// 保存图片
        /// </summary>
        /// <param name="path"></param>
        /// <param name="name"></param>
        /// <param name="subfix"></param>
        /// <param name="contentType"></param>
        /// <param name="fileStream"></param>
        /// <returns></returns>
        int SaveImage(string path, string name, string subfix, string contentType, Stream fileStream);

        /// <summary>
        /// 保存图片
        /// </summary>
        /// <param name="path"></param>
        /// <param name="name"></param>
        /// <param name="subfix"></param>
        /// <param name="hashValue"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        int SaveImage(string path, string name, string subfix, string hashValue, int width, int height);

        /// <summary>
        /// 获取图片
        /// </summary>
        /// <param name="imageId"></param>
        /// <returns></returns>
        ImageModel GetImage(int imageId);
    }
}