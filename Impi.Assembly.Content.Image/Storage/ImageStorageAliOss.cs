﻿using Aliyun.OSS;
using System.IO;

namespace Impi.Assembly.Content.Image
{
    /// <summary>
    /// 图片物理存储阿里OSS实现
    /// </summary>
    public class ImageStorageAliOss : ImageStorageImpl
    {
        private ImageConfigImpl _imageConfig = null;

        public ImageStorageAliOss(ImageConfigImpl imageConfig)
        {
            _imageConfig = imageConfig;
        }

        /// <summary>
        /// 物理保存图片
        /// </summary>
        /// <param name="name"></param>
        /// <param name="contentType"></param>
        /// <param name="fileStream"></param>
        public void SaveImage(string name, string contentType, Stream fileStream)
        {
            var ossClient = new OssClient(_imageConfig.AliOssEndpoint, _imageConfig.AliOssAccessKeyId, _imageConfig.AliOssAccessKeySecret);
            var uploadResult = ossClient.PutObject(_imageConfig.AliOssImageBucketName, name, fileStream, new ObjectMetadata()
            {
                ContentType = contentType
            });
        }
    }
}