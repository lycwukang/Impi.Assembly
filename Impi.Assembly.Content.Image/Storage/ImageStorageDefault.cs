﻿namespace Impi.Assembly.Content.Image
{
    /// <summary>
    /// 图片物理存储默认实现
    /// </summary>
    public class ImageStorageDefault : ImageStorageAliOss
    {
        public ImageStorageDefault(ImageConfigImpl imageConfig) : base(imageConfig)
        {
        }
    }
}