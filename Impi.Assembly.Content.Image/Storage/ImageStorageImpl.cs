﻿using System.IO;

namespace Impi.Assembly.Content.Image
{
    /// <summary>
    /// 图片物理存储
    /// </summary>
    public interface ImageStorageImpl
    {
        /// <summary>
        /// 物理保存图片
        /// </summary>
        /// <param name="name"></param>
        /// <param name="contentType"></param>
        /// <param name="fileStream"></param>
        void SaveImage(string name, string contentType, Stream fileStream);
    }
}