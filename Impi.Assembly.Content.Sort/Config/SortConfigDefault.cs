﻿using System;

namespace Impi.Assembly.Content.Sort
{
    /// <summary>
    /// 排序配置
    /// </summary>
    public class SortConfigDefault : SortConfigImpl
    {
        /// <summary>
        /// 数据库配置名称
        /// </summary>
        public string DbName { get { return "content"; } }

        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get { return "content_sort"; } }
    }
}