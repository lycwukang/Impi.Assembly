﻿namespace Impi.Assembly.Content.Sort
{
    /// <summary>
    /// 排序配置
    /// </summary>
    public interface SortConfigImpl
    {
        /// <summary>
        /// 数据库配置名称
        /// </summary>
        string DbName { get; }

        /// <summary>
        /// 表名
        /// </summary>
        string TableName { get; }
    }
}