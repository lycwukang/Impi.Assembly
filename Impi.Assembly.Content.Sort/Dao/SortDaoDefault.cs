﻿namespace Impi.Assembly.Content.Sort
{
    /// <summary>
    /// 排序数据访问默认实现
    /// </summary>
    public class SortDaoDefault : SortDaoMySql
    {
        public SortDaoDefault(SortConfigImpl sortConfig) : base(sortConfig)
        {
        }
    }
}