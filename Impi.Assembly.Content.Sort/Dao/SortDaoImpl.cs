﻿namespace Impi.Assembly.Content.Sort
{
    /// <summary>
    /// 排序数据访问
    /// </summary>
    public interface SortDaoImpl
    {
        /// <summary>
        /// 保存排序数据
        /// </summary>
        /// <param name="sortData"></param>
        void SaveSort(SortDataModel sortData);

        /// <summary>
        /// 获取排序数据
        /// </summary>
        /// <param name="contentType"></param>
        /// <param name="contentId"></param>
        /// <returns></returns>
        SortDataModel GetSort(string contentType, string contentId);
    }
}