﻿using Impi.Assembly.Core.Data;

namespace Impi.Assembly.Content.Sort
{
    /// <summary>
    /// 排序数据访问MySql实现
    /// </summary>
    public class SortDaoMySql : SortDaoImpl
    {
        private SortConfigImpl _sortConfig = null;

        public SortDaoMySql(SortConfigImpl sortConfig)
        {
            _sortConfig = sortConfig;
        }

        /// <summary>
        /// 获取排序数据
        /// </summary>
        /// <param name="contentType"></param>
        /// <param name="contentId"></param>
        /// <returns></returns>
        public SortDataModel GetSort(string contentType, string contentId)
        {
            var sql = $@"SELECT * FROM `{_sortConfig.TableName}` WHERE `ContentType`=@ContentType AND `ContentId`=@ContentId LIMIT 1;";
            var command = Commands.GetCommand(sql, _sortConfig.DbName);
            return command.Read<SortDataModel>(new
            {
                ContentType = contentType,
                ContentId = contentId
            });
        }

        /// <summary>
        /// 保存排序数据
        /// </summary>
        /// <param name="sortData"></param>
        public void SaveSort(SortDataModel sortData)
        {
            var sql = $@"INSERT INTO `{_sortConfig.TableName}`(`ContentType`, `ContentId`, `SortAttr`, `CreatedAt`)
SELECT @ContentType, @ContentId, @SortAttr, @CreatedAt FROM DUAL WHERE (SELECT COUNT(1) FROM `{_sortConfig.TableName}` WHERE `ContentType`=@ContentType AND `ContentId`=@ContentId)=0;
UPDATE `{_sortConfig.TableName}` SET `SortAttr`=@SortAttr WHERE `ContentType`=@ContentType AND `ContentId`=@ContentId;";
            var command = Commands.GetCommand(sql, _sortConfig.DbName);
            command.Exec(new
            {
                ContentType = sortData.ContentType,
                ContentId = sortData.ContentId,
                SortAttr = sortData.SortAttr,
                CreatedAt = sortData.CreatedAt
            });
        }
    }
}