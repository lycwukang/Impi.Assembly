﻿using System;

namespace Impi.Assembly.Content.Sort
{
    public class SortDataModel
    {
        /// <summary>
        /// 自增长编号
        /// </summary>
        public int SortId { get; set; }

        /// <summary>
        /// 内容类型
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        /// 内容编号
        /// </summary>
        public string ContentId { get; set; }

        /// <summary>
        /// 排序内容数组
        /// </summary>
        public string SortAttr { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreatedAt { get; set; }
    }
}