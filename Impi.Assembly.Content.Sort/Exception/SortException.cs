﻿using System;

namespace Impi.Assembly.Content.Sort
{
    public class SortException : Exception
    {
        public SortException(string message) : base(message)
        {
        }
    }
}