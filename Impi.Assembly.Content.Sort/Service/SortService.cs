﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Impi.Assembly.Content.Sort
{
    /// <summary>
    /// 排序服务接口默认实现
    /// </summary>
    public class SortService : SortServiceImpl
    {
        private SortDaoImpl _sortDao = null;

        public SortService(SortDaoImpl sortDao)
        {
            _sortDao = sortDao;
        }

        /// <summary>
        /// 获取排序内容
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="contentType"></param>
        /// <param name="contentId"></param>
        /// <returns></returns>
        public List<G> GetSort<T, F, G>(T contentType, F contentId)
        {
            var sortData = _sortDao.GetSort(JsonConvert.SerializeObject(contentType), JsonConvert.SerializeObject(contentId));
            if (sortData == null)
            {
                return new List<G>();
            }
            return JsonConvert.DeserializeObject<List<G>>(sortData.SortAttr);
        }

        /// <summary>
        /// 保存内容排序
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="contentType"></param>
        /// <param name="contentId"></param>
        /// <param name="sortAttr"></param>
        public void SetSort<T, F, G>(T contentType, F contentId, List<G> sortAttr)
        {
            _sortDao.SaveSort(new SortDataModel()
            {
                ContentType = JsonConvert.SerializeObject(contentType),
                ContentId = JsonConvert.SerializeObject(contentId),
                SortAttr = JsonConvert.SerializeObject(sortAttr),
                CreatedAt = DateTime.Now
            });
        }
    }
}