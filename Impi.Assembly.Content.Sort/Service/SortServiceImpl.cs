﻿using System;
using System.Collections.Generic;

namespace Impi.Assembly.Content.Sort
{
    /// <summary>
    /// 排序服务接口
    /// </summary>
    public interface SortServiceImpl
    {
        /// <summary>
        /// 保存内容排序
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="F"></typeparam>
        /// <typeparam name="G"></typeparam>
        /// <param name="contentType"></param>
        /// <param name="contentId"></param>
        /// <param name="sortAttr"></param>
        void SetSort<T, F, G>(T contentType, F contentId, List<G> sortAttr);

        /// <summary>
        /// 获取排序内容
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="F"></typeparam>
        /// <typeparam name="G"></typeparam>
        /// <param name="contentType"></param>
        /// <param name="contentId"></param>
        /// <returns></returns>
        List<G> GetSort<T, F, G>(T contentType, F contentId);
    }
}