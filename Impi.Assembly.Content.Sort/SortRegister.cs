﻿using Impi.Assembly.Core.Ioc;

namespace Impi.Assembly.Content.Sort
{
    /// <summary>
    /// 注册模块
    /// </summary>
    public static class SortRegister
    {
        /// <summary>
        /// 创建模块
        /// </summary>
        /// <param name="iocContainer"></param>
        public static void Register(IocContainer iocContainer)
        {
            if (!iocContainer.IsRegister<SortConfigImpl>()) iocContainer.RegisterType<SortConfigDefault, SortConfigImpl>();
            if (!iocContainer.IsRegister<SortDaoImpl>()) iocContainer.RegisterType<SortDaoDefault, SortDaoImpl>();

            if (!iocContainer.IsRegister<SortServiceImpl>()) iocContainer.RegisterType<SortService, SortServiceImpl>();
        }
    }
}