﻿using System.Configuration;

namespace Impi.Assembly.Core.Data
{
    internal class ConnectionConfigHelper
    {
        internal static string GetConfigConnection(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }

        internal static string GetConfigProviderName(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ProviderName;
        }
    }
}