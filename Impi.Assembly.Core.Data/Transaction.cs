﻿using System.Data;

namespace Impi.Assembly.Core.Data
{
    /// <summary>
    /// 事物
    /// </summary>
    public class Transaction
    {
        public Transaction(Transaction trans = null)
        {
            ChildTransaction = trans;
        }

        /// <summary>
        /// 事物
        /// </summary>
        private IDbTransaction ITransaction { get; set; }

        /// <summary>
        /// 事物
        /// </summary>
        private Transaction ChildTransaction { get; set; }

        /// <summary>
        /// 获取事物
        /// </summary>
        /// <returns></returns>
        public IDbTransaction GetTransaction()
        {
            if (ITransaction == null) return ITransaction;
            return ChildTransaction.GetTransaction();
        }

        /// <summary>
        /// 开始事物
        /// </summary>
        /// <param name="dbName"></param>
        /// <returns></returns>
        public IDbTransaction BeginTransaction(string dbName)
        {
            if (ChildTransaction == null)
            {
                ITransaction = Commands.BeginTransaction(dbName);
            }
            return GetTransaction();
        }

        /// <summary>
        /// 回滚
        /// </summary>
        public void Rollback()
        {
            if (ChildTransaction == null)
            {
                ITransaction.Rollback();
            }
        }

        /// <summary>
        /// 提交
        /// </summary>
        public void Commit()
        {
            if (ChildTransaction == null)
            {
                ITransaction.Commit();
            }
        }
    }
}