﻿using Autofac;
using System;

namespace Impi.Assembly.Core.Ioc
{
    /// <summary>
    /// 全局Ioc容器
    /// </summary>
    public class IocContainer
    {
        /// <summary>
        /// 唯一容器
        /// </summary>
        private IContainer _container { get; set; } = new ContainerBuilder().Build();

        /// <summary>
        /// 注册反转
        /// </summary>
        /// <param name="delegate"></param>
        /// <returns></returns>
        public IocContainer Register(Action<IocContainer> @delegate)
        {
            @delegate(this);
            return this;
        }

        /// <summary>
        /// 注册类型
        /// </summary>
        /// <typeparam name="TImplementer"></typeparam>
        /// <typeparam name="IService"></typeparam>
        public IocContainer RegisterType<TImplementer, IService>()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<TImplementer>().As<IService>();
            builder.Update(_container);

            return this;
        }

        /// <summary>
        /// 是否注册类型
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <returns></returns>
        public bool IsRegister<TService>()
        {
            return _container.IsRegistered<TService>();
        }

        /// <summary>
        /// 获取注册类型
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <returns></returns>
        public TService Resolve<TService>()
        {
            return _container.Resolve<TService>();
        }
    }
}